from src.main import main
import requests
import pytest


#first test method
def test_first():
    url = 'https://the-internet.herokuapp.com/context_menu'
    try:
        response = requests.get(url).text
    except requests.ConnectionError as exception:
        print("Invalid URL")

    substr ="Right-click in the box below to see one called 'the-internet'"
    #chekcing if substr is a substring of response
    assert(substr in response)
